package  models;

import play.db.ebean.Model;

import javax.persistence.*;
import models.StockItem;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Warehouse extends Model{
    @Id
    public Long id;

    public String name;

    @OneToOne
    public Address address;

    @OneToMany(mappedBy = "warehouse")
    public List<StockItem> stock = new ArrayList();//truong quan he

    public static Finder<Long, Warehouse> find = new Finder<>(Long.class, Warehouse.class);

    public static Warehouse findById(Long id){
        return find.byId(id);
    }

    public String toString(){
        return name;
    }
}