package models;

import play.db.ebean.Model;
import javax.persistence.*;
import models.Warehouse;
import models.Product;
import java.lang.*;

@Entity
public class StockItem extends Model{
    @Id
    public Long id;

    @ManyToOne
    public Warehouse warehouse;//truong quan he noi voi warehouse

    @ManyToOne
    public Product product;//truong quan he noi voi Product

    public Long quantity;

    public static Finder<Long, StockItem> find = new Finder<>(Long.class, StockItem.class);

    public static StockItem findById(Long id){
        return find.byId(id);
    }
    public String toString(){
        return String.format("StockIem %d - %d x product %s\n",id,quantity,product==null ? null : product.id);
    }
}