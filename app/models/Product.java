package models;

import play.data.validation.Constraints;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;
import play.data.format.Formats;
import play.db.ebean.Model;
import javax.persistence.*;
import java.util.*;
import com.avaje.ebean.*;


@Entity
public class Product extends Model implements PathBindable<Product> {
    private static List<Product> products;

    @Id
    public Long id;

    @Constraints.Required
    public String ean;

    @Constraints.Required
    public String name;
    public String description;

    //@Constraints.Required
    //@DateFormat(pattern = "yyyy-MM-dd")
    public Date date;

    /*@Constraints.Required
    @DateFormat("yyyy-MM-dd")*/

    @OneToMany(mappedBy="product")
    public List<StockItem> stockItem;

    public byte[] picture;
    @ManyToMany
    public List<Tag> tags;//truong quan he noi voi tac

    public Product(){}
    public Product(String ean,String name,String description){
        this.ean=ean;
        this.name=name;
        this.description=description;
    }

    public static Finder<Long,Product> find = new Finder<Long,Product>(long.class,Product.class);

    /*public static List<Product> findAll(){
        return find.all();
    }*/

    public static Page<Product> find(int page){
        return find.where()
                .orderBy("id asc")//sap tang dan theo id
                .findPagingList(10)//quy dinh kich thuoc trang
                .setFetchAhead(false)//co can lay tat ca du lieu 1 the
                .getPage(page);//lay trang hien tai bat dau tu trang 0
    }

    public static Product findByEan(String ean){
        return find.where().eq("ean", ean).findUnique();
    }

    @Override
    public Product bind(String key, String value){
        return findByEan(value);
    }

    @Override
    public String unbind(String key){
        return ean;
    }

    @Override
    public String javascriptUnbind(){
        return ean;
    }
     /*public void save(){
        products.remove(findByEan(this.ean));
        products.add(this);
    }*/
    /*public static List<Product> findByName(String term){
        final List<Product> results=new ArrayList<Product>();
        for(Product candidate : products){
            if(candidate.name.toLowerCase().contains(term.toLowerCase())){
                results.add(candidate);
            }
        }
        return results;
    }*/
}
